from app import app, db
from app.models import Link
from app.auth import requires_auth
from flask import render_template, request, redirect


@app.route('/<short_url>')
def redirect_url(short_url):
    link = Link.query.filter_by(short_url=short_url).first_or_404()
    link.visits += 1
    db.session.commit()
    return redirect(link.original_url)

@app.route('/')
@requires_auth
def index():
    return render_template('index.html')

@app.route('/add_link', methods=['POST'])
@requires_auth
def add_link():
    original_url = request.form.get('original_url')
    link = Link(original_url=original_url)
    db.session.add(link)
    db.session.commit()

    return render_template('link_added.html',
        new_link=link.short_url, original_url=link.original_url)

@app.errorhandler(404)
def page_not_found(e):
    return '', 404
